#!/usr/bin/python

import os, sys, csv
from serial import Serial
import time
from socketIO_client import SocketIO
import json, random

slots = {}

def get_player_name(code):
	with open("../prox.csv", "r") as people:
		data = csv.reader(people)
		for row in data:
			if row[0] == code:
				return "%s %s" % (row[1], row[2])
	return None

def load_leader_board():
	data = []
	with open('../data.csv', 'r') as f:
		csvdata = csv.reader(f)
		for row in csvdata:
			data.append((row[0],row[1]))
	return data

def update_leader_board(data, score):
	s = [x for x in data if score[1] == x[1]]				
	if len(s) > 0:
		data.remove(s[0])
	data.append(score)
	data.sort()
	data.reverse()
	return data.index(score) + 1

def save_leader_board(data):
	with open('../data.csv', 'wb') as csvfile:
		writer = csv.writer(csvfile)
		for row in data:
			writer.writerow(row)
	return data

def reseed_slots():
	data = [100,200,300,400,500,600]
	keys = ("0","1","2","3","4","5")
	for k in keys:
		item = random.choice(data)
		slots[k] = item
		data.remove(item)

if __name__ == "__main__":
	player_number = 1
	player_name = None 
	scores = load_leader_board()
	slots = {}
	score = 0
	count = 0
	max_count = 10
	socketIO = SocketIO('10.38.13.9', 8080)
	print "socket open"
	s = Serial(port="/dev/ttyACM0", baudrate=2400)
	time.sleep(1)
	print "serial ready"
	reseed_slots()
	is_reading_card = False
	player_name_buffer = ""
	try:
		while True:
			m = s.read()
			if m == "W":
				if is_reading_card:
					is_reading_card = False
					player_name = get_player_name(player_name_buffer)
					if player_name == None:
						player_name = "Player %d" % player_number
						player_number = player_number + 1
					socketIO.emit('start', {'name':player_name})	
				else:
					player_name_buffer = ""
					is_reading_card = True
			else:
				if is_reading_card:
					# read card data
					player_name_buffer = player_name_buffer + m
				else:
					if player_name == None:
						socketIO.emit('msg', {'msg':'Please login'})
						continue
					print m
					# play as normal
					score = score + slots[m]
					count = count + 1
					socketIO.emit('score', {'score': score, 'chip': count, 'max_chip': max_count})
					if max_count == count:
						# save score
						rank = update_leader_board(scores, (score, player_name))
						socketIO.emit('game_over', {'final_score':score, 'leaders': json.dumps(scores), 'rank':rank});
						count = 0
						score = 0
						player_name = None
						reseed_slots()

	except KeyboardInterrupt:
		pass
	save_leader_board(scores)
	s.close()
	print "done"
