$(function(){
});	
function show_message(msg) {
	$(".gamestart .gamestart_message").html(msg);
	$(".gamestart").show("slide", { direction: "left" }, 1000);
	setTimeout(function(){
		$(".gamestart").hide("slide", { direction: "right" }, 1000);
	}, 2000);
}
var socket = io.connect('http://10.38.13.9:8080');
	socket.on('msg', function(data) {
		show_message(data.msg);
	});
	socket.on('start', function(data) {
		show_message("Welcome, " + data.name + "<br/>Let's play Plinko!");
	});
	socket.on('game_over', function(data) {
		//data = eval(data);

		$(".gameover .gameover_score").text(data.final_score);
		$(".gameover .gameover_rank").text(data.rank);
		$(".gameover").show("slide", { direction: "left" }, 1000);
		setTimeout(function(){
			$(".gameover").hide("slide", { direction: "right" }, 1000);
		}, 5000);
		// clear the board
		setScore(0);
		$("#chips").text("Ready to begin");

		// update high score
		$("#leader_board table tbody").empty();
		leaders = eval(data.leaders);

		for(var i=0;i<leaders.length;i++) {
			$("#leader_board table tbody").append("<tr><td>" + leaders[i][1] + "</td><td>"+ leaders[i][0] + "</td></tr>");
		}	
	});
	socket.on('update', function(data) {
		setScore(data.score);
		if (data.chip == data.max_chip) {
			$("#chips").text("Game over, man!");
			var snd = new Audio("gameover.mp3");
			snd.play();
		} else if (data.chip == data.max_chip-1) {
			new Audio("coin.mp3").play();
			$("#chips").text("Last chip! Make it count!");
		} else {
			new Audio("coin.mp3").play();
			$("#chips").text((data.max_chip-data.chip) + " chips remaining");
		}
	});
	function setScore(score) {
		var thousands = parseInt(score / 1000, 10);
		score -= (thousands*1000);
		var hundreds = parseInt(score / 100, 10);
		score -= (hundreds*100);
		var tens = parseInt(score / 10, 10);
		var ones = score - (tens*10);
		$("ul.thousands li:first-child a div.inn").text(thousands);
		$("ul.hundreds li:first-child a div.inn").text(hundreds);
		$("ul.tens li:first-child a div.inn").text(tens);
		$("ul.ones li:first-child a div.inn").text(ones);
	}
