var io = require('socket.io').listen(8080);

io.sockets.on('connection', function (socket) {
	socket.on('msg', function (data) {
		socket.broadcast.emit('msg', data);
	});
	socket.on('start', function (data) {
		socket.broadcast.emit('start', data);
	});
	socket.on('score', function (data) {
		console.log(data);
		socket.broadcast.emit('update', data);
	});
	socket.on('game_over', function(data) {
		socket.broadcast.emit('game_over', data);
	});
	socket.on('ready', function (data) {
		console.log('client is ready');
	});
});
