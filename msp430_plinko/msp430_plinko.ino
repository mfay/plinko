#define slot0 P2_0
#define slot1 P2_1
#define slot2 P2_2
#define slot3 P2_3
#define slot4 P2_4
#define slot5 P2_5
#define led P1_0
#define w0 P1_4
#define w1 P1_5

volatile unsigned long marker = 0;
volatile int slot0_marker = 0;
volatile int slot1_marker = 0;
volatile int slot2_marker = 0;
volatile int slot3_marker = 0;
volatile int slot4_marker = 0;
volatile int slot5_marker = 0;
int buffer[26];
int counter = 0;

void setup() {
  Serial.begin(2400);

  pinMode(slot0, INPUT);
  attachInterrupt(slot0, slot0_trigger, FALLING);
  pinMode(slot1, INPUT);
  attachInterrupt(slot1, slot1_trigger, FALLING);
  pinMode(slot2, INPUT);
  attachInterrupt(slot2, slot2_trigger, FALLING);
  pinMode(slot3, INPUT);
  attachInterrupt(slot3, slot3_trigger, FALLING);
  pinMode(slot4, INPUT);
  attachInterrupt(slot4, slot4_trigger, FALLING);
  pinMode(slot5, INPUT);
  attachInterrupt(slot5, slot5_trigger, FALLING);
  pinMode(w0, INPUT);
  attachInterrupt(w0, wiegand0, RISING);
  pinMode(w1, INPUT);
  attachInterrupt(w1, wiegand1, RISING);

  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);
  delay(2000);
  digitalWrite(led, LOW);
}

void loop() {
  if (counter >= 26) {
    // process data
    int serial = 0;
    for (int i=9;i<24;i++) {
      serial |= buffer[i];
      serial = (serial << 1);
    }
    serial |= buffer[24];
    Serial.print('W');
    Serial.print(serial);
    Serial.print('W');
    counter = 0;
  } 
  else {
    while (slot0_marker > 0) {
      slot0_marker--;
      Serial.print("0");
    }
    while (slot1_marker > 0) {
      slot1_marker--;
      Serial.print("1");
    }
    while (slot2_marker > 0) {
      slot2_marker--;
      Serial.print("2");
    }
    while (slot3_marker > 0) {
      slot3_marker--;
      Serial.print("3");
    }
    while (slot4_marker > 0) {
      slot4_marker--;
      Serial.print("4");
    }
    while (slot5_marker > 0) {
      slot5_marker--;
      Serial.print("5");
    }
  }
}

void slot0_trigger() {
  slot0_marker++;
}
void slot1_trigger() {
  slot1_marker++;
}
void slot2_trigger() {
  slot2_marker++;
}
void slot3_trigger() {
  slot3_marker++;
}
void slot4_trigger() {
  slot4_marker++;
}
void slot5_trigger() {
  slot5_marker++;
}

void fire() {
  digitalWrite(led, HIGH);
  marker = millis();
}

void wiegand0() {
  buffer[counter] = 0;
  counter++;
}
void wiegand1() {
  buffer[counter] = 1;
  counter++;
}

